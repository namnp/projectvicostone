namespace Project_Vicostone.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "laguage", c => c.String(maxLength: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contacts", "laguage");
        }
    }
}
