﻿using System.Collections.Generic;
using Project_Vicostone.Models;

namespace Project_Vicostone.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Project_Vicostone.Models.QuanLyAccountDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Project_Vicostone.Models.QuanLyAccountDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var hues = new List<Models.Hues>
            {
                new Hues {Id = 1,ImageColor = "/images/color-hues/white.png"},
                new Hues {Id = 2,ImageColor = "/images/color-hues/grey.png"},
                new Hues {Id = 3,ImageColor = "/images/color-hues/light-grey.png"},
                new Hues {Id = 4,ImageColor = "/images/color-hues/black.png"},
                new Hues {Id = 5,ImageColor = "/images/color-hues/brown.png"},
                new Hues {Id = 6,ImageColor = "/images/color-hues/green.png"},
                new Hues {Id = 7,ImageColor = "/images/color-hues/red.png"},
            };
            hues.ForEach(s => context.Hueses.Add(s));
            context.SaveChanges();

            var category=new List<Models.Category>
            {
                new Category {IdCategory = 1,NameCategory = "White"},
                new Category {IdCategory = 2,NameCategory = "Grey"},
                new Category {IdCategory = 3,NameCategory = "Light-grey"},
                new Category {IdCategory = 4,NameCategory = "Black"},
                new Category {IdCategory = 5,NameCategory = "Brown"},
                new Category {IdCategory = 6,NameCategory = "Green"},
                new Category {IdCategory = 7,NameCategory = "Red"},
                new Category {IdCategory = 8,NameCategory = "New Products"},
                new Category {IdCategory = 9,NameCategory = "Classic"},
                new Category {IdCategory = 10,NameCategory = "Natural"},
                new Category {IdCategory = 11,NameCategory = "Exotic"},
                new Category {IdCategory = 12,NameCategory = "All Collections"},
            };
            category.ForEach(s=>context.Categories.Add(s));
            context.SaveChanges();


            var laguages = new List<Language>
            {
                new Language {Id = "vi", Name = "Tiếng Việt", IsDefaul = true},
                new Language {Id = "en", Name = "Tiếng Anh", IsDefaul = false}
            };
            laguages.ForEach(s => context.Languages.Add(s));
            context.SaveChanges();
        }
    }
}
