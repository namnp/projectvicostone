﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project_Vicostone.Common
{
    public class UserLogin
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}