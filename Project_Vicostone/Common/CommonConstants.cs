﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project_Vicostone.Common
{
    public static class CommonConstants
    {
        public static string USER_SESSION = "USER_SESSION";

        public static string CurrentCulture { get; set; }
    }

}