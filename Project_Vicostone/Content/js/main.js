$(document).ready(function () {
    var globalActiveTab = "";
	var openCart = false;
	var cartClick = false;
    $("ul.dropdown-menu").each(function (index) {
        if ($(this).find('li').length == 0) {
            $(this).remove();
        }
    });
    var minicart = $('.minicart');
    minicart.hide();
    function Linkactive(item) {
        var li = item.children('li');
        var link = li.children('a');
        link.click(function () {
            var parent = $(this).parent('li');
            li.removeClass('active');
            link.removeClass('active');
            parent.addClass('active');
        });
    }
    //Linkactive($('.nav')); HaiVM fix bug lose active when click child tab
    Linkactive($('.header .languague'));
    //Linkactive($('.filter-selects > div > ul'));
    Linkactive($('.sidebar > .block > ul'));

	$('.cart-link').click(function(e){
		cartClick = true;
		if ("ontouchstart" in document.documentElement){
			if(openCart==false){
				if($(this).next().find('tr[requestproduct]').length==0){
					location.href='/Request Sample';
				}else{				
					openCart = true;
				}
			}else{
				location.href='/Request Sample';
			}
		}else{
			location.href='/Request Sample';
		}
	});
    $('.tooltip-hover').hover(function () {
        $(this).next($('.tooltip')).toggle();
    });

    $('.tooltip-hover').click(function (e) {
        e.preventDefault();
        $(this).next($('.tooltip')).toggle();
    });

    $('.navbar .container-fluid').addClass('container');

    //when have not banner
    if ($('.banner').length <= 0) {
        $('body').addClass('no-banner');
    }

    //Sticky navigation
    if (jQuery('.header').length) {
        // grab the initial top offset of the navigation
        var elementSticky = $('.header');
        var stickyNavTop = elementSticky.offset().top;
        // our function that decides weather the navigation bar should have "fixed" css position or not.
        var stickyNav = function () {
            var scrollTop = jQuery(window).scrollTop(); // our current vertical position from the top

            // if we've scrolled more than the navigation, change its position to fixed to stick to top,
            // otherwise change it back to relative
            if (scrollTop < stickyNavTop) {
                elementSticky.addClass('sticky');
           } else {
                elementSticky.removeClass('sticky');
            }
			if(scrollTop == 0){
				$(".header").removeClass('sticky');
			}
			stickyNavTop = scrollTop;
        };
        stickyNav();
        // and run it again every time you scroll
        jQuery(window).scroll(function () {
            stickyNav();
        });
   }
	/* $(window).scroll(
	{
		previousTop: 0
	}, 
	function () {
		var currentTop = $(window).scrollTop();
		if (currentTop < this.previousTop) {
			$(".header").addClass('sticky');
			$(".header").show();
		} else {
			$(".header").removeClass('sticky');
			$(".header").hide();
		}
		if(currentTop == 0){
				$(".header").removeClass('sticky');
			}
		this.previousTop = currentTop;
	}); */

    $('.header [data-toggle="collapse"]').click(function () {
        var ex = $('.navbar-collapse').hasClass('in');
        if (ex != null) {
            $('.navbar-mobile .navbar-header').toggleClass('in');
        }
        $('html, body').animate({ scrollTop: 0 }, 'slow');

    });


    //Filter
    $('.btn-filter').click(function () {
        $(this).toggleClass('expand');
        $('.filter-selects').slideToggle(500);
    });

    //Show banner and tab
    $('.banner-d').hide();
    $('.nav-tabs > li > a').click(function () {
        $(".search-product").val("");
        globalActiveTab = $(this).text().trim().toUpperCase();
        var data = $(this).attr('data-click');
        var tabLink = $(this).attr('aria-controls');
        $('.banner-d').each(function () {
            var databanner = jQuery(this).attr('data-click');
            if (databanner == data) {
                $('.banner-d').removeClass('show');
                $(this).addClass('show');
            }
        });
        $('.tab-content .tab-pane').each(function () {
            var tabcontent = $(this).attr('id');
            if (tabLink == tabcontent) {
                $('.tab-pane').removeClass('show');
                $('.tab-pane').removeClass('active');
                $(this).addClass('show');
                $(this).addClass('active');
            }
        });

        ClearFilter();
    });

    //Dropdown select option
    var selectItem = $('.dropdown-selects');
    var select_dd_ul = $('.dropdown-selects dd ul');
    var select_dt_a = $('.dropdown-selects dt a');
    select_dd_ul.hide();

    select_dt_a.click(function (e) {
        e.preventDefault();
        if ($(this).attr('onclick') == undefined) {
            var selectParent = $(this).parent();
            selectParent.toggleClass('open');
            selectParent.next('dd').children('ul').toggle();
        }
        selectItem.mouseleave(function () {
            select_dd_ul.hide();
            $(this).children('dt').removeClass('open');
        });
        /* select_dt_a.click(function () {
             if ($(this).attr('onclick') == undefined) {
                 var selectParent = $(this).parent();
                 selectParent.toggleClass('open');
                 selectParent.next('dd').children('ul').toggle();
             }*/

    });

    $(".dropdown-selects dd ul li a").click(function (e) {
        e.preventDefault();
        var text = $(this).html();
        var dd_this = $(this).parent().parent().parent();
        var dt_this = dd_this.parent().find('dt');
        var dt_span = dt_this.children().children('span');
        dt_span.html(text);
        dd_this.children('ul').hide();
        dt_this.removeClass('open');
    });

    //Minicart
    var header_cart = $('.header-cart');
    var link_cart = $('.cart-link');
    //var minicart = $('.minicart');
    minicart.hide();
    header_cart.hover(function (e) {
        //e.preventDefault();
        //link_cart.toggleClass('open');
        minicart.show();
    });
	
	$(document).click(function() {
		if(cartClick==false){
			minicart.hide();
			openCart=false;
		}else{
			cartClick = false;
		}
	});
    header_cart.mouseleave(function () {
		if ("ontouchstart" in document.documentElement){
			// if(openCart==true){
				// minicart.hide();
				// openCart=false;
			// }
		}else{
			minicart.hide();
		}
		//openCart = false;
    });
    //Show all faq item in FAQs page
    $('.faqs-show').click(function () {
        $('.faq-item .collapse').removeClass('in');
        $('.faq-item h4').attr('aria-expanded', 'true');
        $('.faq-item .collapse').addClass('in');
        $('.faq-item h4').removeClass('collapsed');
        $('.faq-item .collapse').css('height', '');

    });
    //sidebar for mobile-table drop-down
    $(".sidebar .sidebar-mobile h4.block-title").click(function () {
        if (window.innerWidth <= 991) {
            if ($(this).hasClass("showed")) {
                $(this).removeClass("showed");
                $(this).next("ul").slideUp();
            } else {
                $(this).addClass("showed");
                $(this).next("ul").slideDown();
            }
            //$(this).next("ul").slideToggle(); 	
            console.log("1zz");
        }
    });

    $(".sidebar .sidebar-mobile > ul > li > a + ul").prev("a").addClass("a-has-sub");
    $(".sidebar .sidebar-mobile > ul > li > a.a-has-sub").after('<i class="fa fa-angle-down "></i>');
    $(".sidebar").on("click", "i.fa", function (e) {
        event.stopPropagation();
        if (window.innerWidth <= 991) {
            if ($(this).hasClass("showed")) {
                $(this).removeClass("showed");
                $(this).next("ul").slideUp();
            } else {
                $(this).addClass("showed");
                $(this).next("ul").slideDown();
            }
        }
    });


    $(window).resize(function () {
        if (window.innerWidth <= 991) {
            $(".sidebar .sidebar-mobile h4.block-title + ul ").hide();
            $(".sidebar .sidebar-mobile h4.block-title").removeClass("showed");
            $(".sidebar .sidebar-mobile > ul > li >  ul").hide();
            $(".sidebar .sidebar-mobile > ul > li > i").removeClass("showed");
        } else {
            $(".sidebar .sidebar-mobile h4.block-title + ul ").show();
            $(".sidebar .sidebar-mobile h4.block-title").addClass("showed");
            $(".sidebar .sidebar-mobile > ul > li >  ul").show();
            $(".sidebar .sidebar-mobile > ul > li > i").addClass("showed");
        }
    });

    $(window).resize();
    $('#filterColorOptionsProduct li a').click(function () {
        $(this).parent().toggleClass('activateColor');
        $(this).toggleClass('active');
        ApplyFilterProduct();
    });
    
    $('#filterSizeOptions li a').click(function () {
        $(this).parent().toggleClass('activateSize');
        $(this).toggleClass('active');
        ApplyFilterProduct();
    });

    $('#filterColorOptions li a').click(function () {
        $(this).parent().toggleClass('activateColor');
        $(this).toggleClass('active');
        ApplyFilter();
    });

    $('#filterTypeOptions li a').click(function () {
        $(this).parent().toggleClass('activateType');
        $(this).toggleClass('active');
        ApplyFilter();
    });

    $('#filterMaterialOptions li a').click(function () {
        $(this).parent().toggleClass('activateMaterial');
        $(this).toggleClass('active');
        ApplyFilterProduct();
    });

    $('.search-productList').keyup(function (event) {
        if (event.keyCode == 13) {
            ClearFilterProduct();
            var valThis = $.trim($(this).val().toLowerCase());
            var count = 0;
            $('.search-item-parent').show();
            $('.active .search-item').each(function() {
                var productName = $(this).attr('productName').toLowerCase();
                var productCode = $(this).attr('productCode').toLowerCase();
                if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
                    $(this).show();
                    $(this).removeClass('item');
                    $(this).addClass('item');
                    count++;
                } else {
                    $(this).hide();
                    $(this).removeClass('item');
                }
            });

            // Hide gallery collection name in case no sub-product found.
            $('.search-item-parent').each(function() {
                var counter = 0;

                $(this).find('.search-item').each(function(i) {
                    if ($(this).attr('style') == undefined || $(this).attr('style').indexOf("display: none") == -1) {
                        counter++;
                    }
                });

                if (counter == 0) {
                    $(this).hide();
                }
            });

            OrganizeGalleryImages();

            if (valThis.trim() == "") {
                $(window).scroll(WindowScroll);
            } else {
                $(window).unbind("scroll");
            }

            DisplayResultSearch(count, valThis);
        }
    });
    
    $('.search-productBtn').click(function () {
        ClearFilterProduct();
        var valThis = $.trim($('.search-productList').val().toLowerCase());
        if (valThis == "enter product name or code" || valThis == "nhập tên hoặc mã sản phẩm") {
            valThis = "";
        }
        var count = 0;
        $('.search-item-parent').show();
        $('.active .search-item').each(function () {
            var productName = $(this).attr('productName').toLowerCase();
            var productCode = $(this).attr('productCode').toLowerCase();
            if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
                $(this).show();
                $(this).removeClass('item');
                $(this).addClass('item');
                count++;
            } else {
                $(this).hide();
                $(this).removeClass('item');
            }
        });

        // Hide gallery collection name in case no sub-product found.
        $('.search-item-parent').each(function () {
            var counter = 0;

            $(this).find('.search-item').each(function (i) {
                if ($(this).attr('style') == undefined || $(this).attr('style').indexOf("display: none") == -1) {
                    counter++;
                }
            });

            if (counter == 0) {
                $(this).hide();
            }
        });

        OrganizeGalleryImages();

        if (valThis.trim() == "") {
            $(window).scroll(WindowScroll);
        } else {
            $(window).unbind("scroll");
        }

        DisplayResultSearch(count, valThis);
    });
    
    $('.search-product').keyup(function (event) {
        if (event.keyCode == 13) {
            ClearFilter();
            var valThis = $.trim($(this).val().toLowerCase());
            var count = 0;
            var totalResult = 0;
            $('.active .search-item-parent').each(function() {
                var countDisplay = 0;
                $(this).find('.search-item').each(function() {
                    var productName = $(this).attr('productName').toLowerCase();
                    var productCode = $(this).attr('productCode').toLowerCase();
                    if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
                        totalResult++;
                        countDisplay++;
                        if (countDisplay <= 6 && count < 1) {
                            $(this).show();
                            $(this).removeClass('hideMenu');
                            var countShowed = 0;
                            $(this).find('div[data-src]').each(function () {
                                countShowed++;
                                $(this).nextAll().remove();
                                //$(this).parent().next().find('.cus-pint').html('');
                                if (countShowed <= 6) {
                                    $(this).parent().append("<img src='" + $(this).attr('data-src') + "' />");
                                    //$(this).parent().next().find('.cus-pint').append("<a href='" + $(this).attr('pintHref') + "' data-pin-do='buttonPin' data-pin-config='above'></a>");
                                    //$(this).attr('src', $(this).attr('data-src'));
                                } else {
                                    //$(this).find('img').attr('src', '');
                                }
                            });
                        } else {
                            $(this).hide();
                            var itemIndex = parseInt($(this).attr('itemIndex'), 10);
                            if (itemIndex >= 6) {
                                $(this).addClass('hideMenu');
                                $(this).find('img').remove();
                            }
                        }
                    }
                });
                if (countDisplay > 0) {
                    if (count < 1) {
                        $(this).show();
                        $(this).removeClass('hideMenu');
                    } else {
                        $(this).hide();
                        $(this).removeClass('hideMenu');
                        $(this).addClass('hideMenu');
                    }
                    count++;
                } else {
                    $(this).hide();
                    $(this).removeClass('hideMenu');
                    $(this).addClass('hideMenu');
                }
            });

            // Hide gallery collection name in case no sub-product found.
            $('.active .search-item-parent').each(function() {
                var counter = 0;

                $(this).find('.search-item').each(function(i) {
                    if ($(this).attr('style') == undefined || $(this).attr('style').indexOf("display: none") == -1) {
                        counter++;
                    }
                });

                if (counter == 0) {
                    $(this).hide();
                }
            });

            OrganizeGalleryImages();

            if (valThis.trim() == "") {
                $(window).scroll(WindowScroll);
            }

            DisplayResultSearch(totalResult, valThis);
        }
        //if (event.keyCode == 10 || event.keyCode == 13)
        //    event.preventDefault();
    });

    $('.search-gallery').click(function (e) {
        ClearFilter();
        var valThis = $('.search-product').val() !== undefined ? $.trim($('.search-product').val().toLowerCase()) : "";
        if (valThis == "enter product name or code" || valThis == "nhập tên hoặc mã sản phẩm") {
            valThis = "";
        }
        var count = 0;
        var totalResult = 0;
        $('.active .search-item-parent').each(function () {
            var countDisplay = 0;
            $(this).find('.search-item').each(function () {
                var productName = $(this).attr('productName').toLowerCase();
                var productCode = $(this).attr('productCode').toLowerCase();
                if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
                    countDisplay++;
                    totalResult++;
                    if (countDisplay <= 6 && count < 1) {
                        $(this).show();
                        $(this).removeClass('hideMenu');
                        var countShowed = 0;
                        $(this).find('div[data-src]').each(function () {
                            countShowed++;
                            $(this).nextAll().remove();
                            //$(this).parent().next().find('.cus-pint').html('');
                            if (countShowed <= 6) {
                                $(this).parent().append("<img src='" + $(this).attr('data-src') + "' />");
                                //$(this).parent().next().find('.cus-pint').append("<a href='" + $(this).attr('pintHref') + "' data-pin-do='buttonPin' data-pin-config='above'></a>");
                                //$(this).attr('src', $(this).attr('data-src'));
                            } else {
                                //$(this).find('img').attr('src', '');
                            }
                        });
                    } else {
                        $(this).hide();
                        var itemIndex = parseInt($(this).attr('itemIndex'), 10);
                        if (itemIndex >= 6) {
                            $(this).addClass('hideMenu');
                            $(this).find('img').remove();
                        }
                    }
                }
            });
            if (countDisplay > 0) {
                if (count < 1) {
                    $(this).show();
                    $(this).removeClass('hideMenu');
                } else {
                    $(this).hide();
                    $(this).removeClass('hideMenu');
                    $(this).addClass('hideMenu');
                }
                count++;
            } else {
                $(this).hide();
                $(this).removeClass('hideMenu');
                $(this).addClass('hideMenu');
            }
        });

        // Hide gallery collection name in case no sub-product found.
        $('.active .search-item-parent').each(function () {
            var counter = 0;

            $(this).find('.search-item').each(function (i) {
                if ($(this).attr('style') == undefined || $(this).attr('style').indexOf("display: none") == -1) {
                    counter++;
                }
            });

            if (counter == 0) {
                $(this).hide();
            }
        });

        OrganizeGalleryImages();

        if (valThis.trim() == "") {
            $(window).scroll(WindowScroll);
        }

        DisplayResultSearch(totalResult, valThis);
    });
    $('.search-productSample').keyup(function (event) {
        if (event.keyCode == 13) {
            var valThis = $.trim($(this).val().toLowerCase());
            if (valThis == "enter product name or code" || valThis == "nhập tên hoặc mã sản phẩm") {
                valThis = "";
            }
            $('.search-item').each(function() {
                var productName = $(this).attr('productName');
                var productCode = $(this).attr('productCode');
                if (productName != undefined) {
                    productName = productName.toLowerCase();
                } else {
                    productName = "";
                }
                if (productCode != undefined) {
                    productCode = productCode.toLowerCase();
                } else {
                    productCode = "";
                }
                if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }
    });

    $('.search-productSampleBtn').click(function (event) {
        var valThis = $.trim($('.search-productSample').val().toLowerCase());
        if (valThis == "enter product name or code" || valThis == "nhập tên hoặc mã sản phẩm") {
            valThis = "";
        }
        $('.search-item').each(function () {
            var productName = $(this).attr('productName');
            var productCode = $(this).attr('productCode');
            if (productName != undefined) {
                productName = productName.toLowerCase();
            } else {
                productName = "";
            }
            if (productCode != undefined) {
                productCode = productCode.toLowerCase();
            } else {
                productCode = "";
            }
            if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    });
    
    $('.btn-clear-filter').click(function (e) {
        ClearFilter();
    });

    function ClearFilter() {
        if ($('.search-productList').length>0) {
            ClearFilterProduct();
            return;
        }
        $('.filterOptions li').each(function () {
            $(this).find('a').removeClass("active");
            $(this).removeClass("activateColor");
            $(this).removeClass("activateSize");
            $(this).removeClass("activateType");
            $(this).removeClass("activateMaterial");
        });
        ApplyFilter();

        ShowDefaultItem();
    }

    function ClearFilterProduct() {
        $('.filterOptions li').each(function () {
            $(this).find('a').removeClass("active");
            $(this).removeClass("activateColor");
            $(this).removeClass("activateSize");
            $(this).removeClass("activateType");
            $(this).removeClass("activateMaterial");
        });
        ApplyFilterProduct();

        ShowDefaultItem();
    }
    
    function ApplyFilter() {
        if ($("#filterSizeOptions li.activateSize a").length>0 && $("#filterSizeOptions li.activateSize a").attr('class') != undefined) {
            var option1 = "";
            $("#filterSizeOptions li.activateSize a").each(function () {
                option1 += " " + $(this).attr('class');
            });
        }
        else {
            var option1 = "";
        }

        if ($("#filterColorOptions li.activateColor a").length>0 && $("#filterColorOptions li.activateColor a").attr('class') != undefined) {
            var option2 = "";
            $("#filterColorOptions li.activateColor a").each(function () {
                option2 += " " + $(this).attr('class');
            });
        } else {
            var option2 = "";
        }
        if ($('#filterColorOptions li.activateColor').length == $('#filterColorOptions li').length-1) {
            option2 = "";
        }

        if ($("#filterTypeOptions li.activateType a").length>0 && $("#filterTypeOptions li.activateType a").attr('class') != undefined) {
            var option3 = "";
            $("#filterTypeOptions li.activateType a").each(function () {
                option3 += " " + $(this).attr('class');
            });
        } else {
            var option3 = "";
        }

        if ($("#filterMaterialOptions li.activateMaterial a").length>0 && $("#filterMaterialOptions li.activateMaterial a").attr('class') != undefined) {
            var option4 = "";
            $("#filterMaterialOptions li.activateMaterial a").each(function () {
                option4 += " " + $(this).attr('class');
            });
        } else {
            var option4 = "";
        }

        option1 = option1.replace(/active/g, "").replace(/  /g, " ").trim();
        option2 = option2.replace(/active/g, "").replace(/  /g, " ").trim();
        option3 = option3.replace(/active/g, "").replace(/  /g, " ").trim();
        option4 = option4.replace(/active/g, "").replace(/  /g, " ").trim();

        var count = 0;
        var countLine = 0;
        var valThis = "";
        if ($('.search-product').length > 0) {
            valThis = $('.search-product').val() !== undefined ? $.trim($('.search-product').val().toLowerCase()) : "";
        }
        if (valThis == "enter product name or code" || valThis == "nhập tên hoặc mã sản phẩm") {
            valThis = "";
        }
        
        $('.active .search-item-parent').each(function () {
            var countDisplay = 0;
            $(this).find('div.search-item').each(function () {
                var filterOptions = $(this).attr('filter-options').toLowerCase();
                if (filterOptions.ContainAnyString(option1) && filterOptions.ContainAnyString(option2) && filterOptions.ContainAnyString(option3) && filterOptions.ContainAnyString(option4)) {
                    if (valThis == "") {
                        countDisplay++;
                    } else {
                        var productName = $(this).attr('productName').toLowerCase();
                        var productCode = $(this).attr('productCode').toLowerCase();
                        if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
                            countDisplay++;
                        }
                    }
                }
            });
            if (countDisplay > 0 && countLine < 1) {
                $(this).show();
                $(this).removeClass("default-show");
                $(this).addClass("default-show");
                $(this).removeClass("hideMenu");
                //countLine++;
            } else {
                $(this).hide();
                $(this).removeClass("default-show");
                $(this).removeClass("hideMenu");
                $(this).addClass("hideMenu");
            }
            if (countDisplay > 6) {
                $(this).find('.loadMoreGallery').show();
            } else {
                $(this).find('.loadMoreGallery').hide();
                if (countDisplay == 0) {
                    $(this).hide();
                }
            }
            var itemShowedCount = 0;
            $(this).find('.search-item').each(function () {
                
                var filterOptions = $(this).attr('filter-options').toLowerCase();
                if (filterOptions.ContainAnyString(option1) && filterOptions.ContainAnyString(option2) && filterOptions.ContainAnyString(option3) && filterOptions.ContainAnyString(option4)) {
                    if (valThis == "") {
                        $(this).show();
                        if (itemShowedCount < 6) {
                            var countShowed = 0;
                            $(this).find('div[data-src]').each(function () {
                                countShowed++;
                                $(this).nextAll().remove();
                                //$(this).parent().next().find('.cus-pint').html('');
                                if (countShowed <= 6) {
                                    if (countLine < 1) {
                                        $(this).parent().append("<img src='" + $(this).attr('data-src') + "' />");
                                        //$(this).parent().next().find('.cus-pint').append("<a href='" + $(this).attr('pintHref') + "' data-pin-do='buttonPin' data-pin-config='above'></a>");
                                    }
                                    //$(this).attr('src', $(this).attr('data-src'));
                                } else {
                                    //$(this).find('img').attr('src', '');
                                }
                            });
                            $(this).removeClass('hideMenu');
                            itemShowedCount++;
                        } else {
                            $(this).removeClass('hideMenu');
                            $(this).addClass('hideMenu');
                            $(this).find('img').each(function () {
                                $(this).remove();
                            });
                        }
                        count++;
                    } else {
                        var productName = $(this).attr('productName').toLowerCase();
                        var productCode = $(this).attr('productCode').toLowerCase();
                        if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
                            $(this).show();
                            if (itemShowedCount < 6) {
                                var countShowed1 = 0;
                                $(this).find('div[data-src]').each(function () {
                                    countShowed1++;
                                    $(this).nextAll().remove();
                                    //$(this).parent().next().find('.cus-pint').html('');
                                    if (countShowed1 <= 6) {
                                        if (countLine < 1) {
                                            $(this).parent().append("<img src='" + $(this).attr('data-src') + "' />");
                                            //$(this).parent().next().find('.cus-pint').append("<a href='" + $(this).attr('pintHref') + "' data-pin-do='buttonPin' data-pin-config='above'></a>");
                                        }
                                        //$(this).attr('src', $(this).attr('data-src'));
                                    } else {
                                        //$(this).find('img').attr('src', '');
                                    }
                                });
                                $(this).removeClass('hideMenu');
                                itemShowedCount++;
                            } else {
                                $(this).removeClass('hideMenu');
                                $(this).addClass('hideMenu');
                                $(this).find('img').each(function () {
                                    $(this).remove();
                                });
                            }
                            count++;
                        }
                    }
                    
                } else {
                    $(this).hide();
                    $(this).removeClass('hideMenu');
                    $(this).addClass('hideMenu');
                    //if (itemIndex >= 6) {
                    $(this).find('img').remove();
                    //}
                }
            });
            countLine++;
        });
        

        $('.active .search-item-parent').each(function () {
            var counter = 0;

            $(this).find('.search-item').each(function (i) {
                if ($(this).attr('style') == undefined || $(this).attr('style').indexOf("display: none") == -1) {
                    counter++;
                }
            });

            if (counter == 0) {
                $(this).hide();
            }
        });
        if (option1.trim() == "" && option2.trim() == "" && option3.trim() == "" && option4.trim() == "") {
            $(window).scroll(WindowScroll);
        }

        DisplayResultLabel(count, option2, option1, option3, option4);

        OrganizeGalleryImages();

        return false;
    };

    function ApplyFilterProduct() {
        if ($("#filterSizeOptions li.activateSize a").length>0 && $("#filterSizeOptions li.activateSize a").attr('class') != undefined) {
            var option1 = "";
            $("#filterSizeOptions li.activateSize a").each(function () {
                option1 += " " + $(this).attr('class');
            });
        }
        else {
            var option1 = "";
        }

        if ($("#filterColorOptionsProduct li.activateColor a").length>0 && $("#filterColorOptionsProduct li.activateColor a").attr('class') != undefined) {
            var option2 = "";
            $("#filterColorOptionsProduct li.activateColor a").each(function () {
                option2 += " " + $(this).attr('class');
            });
        } else {
            var option2 = "";
        }

        if ($("#filterTypeOptions li.activateType a").length>0 && $("#filterTypeOptions li.activateType a").attr('class') != undefined) {
            var option3 = "";
            $("#filterTypeOptions li.activateType a").each(function () {
                option3 += " " + $(this).attr('class');
            });
        } else {
            var option3 = "";
        }

        if ($("#filterMaterialOptions li.activateMaterial a").length>0 && $("#filterMaterialOptions li.activateMaterial a").attr('class') != undefined) {
            var option4 = "";
            $("#filterMaterialOptions li.activateMaterial a").each(function () {
                option4 += " " + $(this).attr('class');
            });
        } else {
            var option4 = "";
        }

        option1 = option1.replace(/active/g, "").replace(/  /g, " ").trim();
        option2 = option2.replace(/active/g, "").replace(/  /g, " ").trim();
        option3 = option3.replace(/active/g, "").replace(/  /g, " ").trim();
        option4 = option4.replace(/active/g, "").replace(/  /g, " ").trim();

        var count = 0;
        $('.search-item-parent').show();
        $('.active .search-item').each(function () {
            var filterOptions = $(this).attr('filter-options').toLowerCase();
            if (filterOptions.ContainAnyString(option1) && filterOptions.ContainAnyString(option2) && filterOptions.ContainAnyString(option3) && filterOptions.ContainAnyString(option4)) {
                $(this).show();
                count++;
            } else {
                $(this).hide();
            }
        });

        $('.search-item-parent').each(function () {
            var counter = 0;

            $(this).find('.search-item').each(function (i) {
                if ($(this).attr('style') == undefined || $(this).attr('style').indexOf("display: none") == -1) {
                    counter++;
                }
            });

            if (counter == 0) {
                $(this).hide();
            }
        });

        if (option1.trim() == "" && option2.trim() == "" && option3.trim() == "" && option4.trim() == "") {
            $(window).scroll(WindowScroll);
        } else {
            $(window).unbind("scroll");
        }

        DisplayResultLabel(count, option2, option1, option3, option4);

        OrganizeGalleryImages();

        return false;
    };
    
    function OrganizeGalleryImages() {
        if ($('.gridMasonry').length > 0) {

            var $container = $('.gridMasonry');
            $container.imagesLoaded(function () {
                $container.masonry();
                $('.active').find(".search-item-parent").each(function() {
                    $(this).css('opacity', '');
                    $(this).find('.search-item').not('.hideMenu').css('opacity', '');
                });
                $(".loader").hide();
                //$(".active").find("div.search-item-parent").each(function () {
                //    if ($(this).attr('style')==undefined || $(this).attr('style').indexOf('none') == -1 || $(this).attr('style').indexOf('display') == -1) {
                //        $(this).removeClass('hideMenu');
                //    }
                //});
            });
        } else {
            $(".loader").hide();
        }
    }

    function DisplayResultLabel(count, hue, size, type, material, tab) {
        $('.active .amount').text("");
        if (hue == undefined || hue == "") {
            hue = "";
        } else {
            hue = "- Hue: " + hue.toUpperCase();
        }

        if (size == undefined || size == "") {
            size = "";
        } else {
            size = "- Size: " + size.toUpperCase();
        }

        if (type == undefined || type == "") {
            type = "";
        } else {
            type = "- Type: " + type.toUpperCase();
        }

        if (material == undefined || material == "") {
            material = "";
        } else {
            material = " - Material: " + material.toUpperCase();
        }

        if (hue == "" && size == "" && type == "" && material == "") {
            if (globalActiveTab != "") {
                hue = globalActiveTab;
            } else {
                hue = $(".nav-tabs .active a").text().trim().toUpperCase();
            }
        }

        var resultString = '{0} Results for: {1} {2} {3} {4}'.format(count, hue, size, material, type);
        $('.active .amount').append(resultString);
        return false;
    };

    function DisplayResultSearch(count, searchText) {
        $('.active .amount').text("");
        var resultString = '{0} Results for: Search text: "{1}"'.format(count, searchText.toUpperCase());
        $('.active .amount').append(resultString);
        return false;
    };

    $('.btn-download-file').click(function (e) {
        var fileUrl = $(this).attr('download-file');
        window.open(window.location.origin + fileUrl, '_blank');
    });

    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/\{(\d+)\}/g, function (m, n) { return args[n]; });
    };

    String.prototype.ContainAnyString = function (classes) {
        var result = false;
        var args = classes.split(" ");
        for (var i = 0, l = args.length; i < l; i++) {
            result = this.indexOf(args[i]) > -1;
            if (result) {
                return result;
            }
        }
        return result;
    };

    $('.btn-delete-image').click(function (e) {
        var fileUrl = $(this).attr('fileUrl');
        if (confirm("Are you sure you want to delete this image?")) {
            $.ajax({
                type: "POST",
                url: "/Webservices/Services.asmx/DeleteServerFile",
                data: "{'fileUrl' : '" + fileUrl + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    location.reload();
                }
            });
        }
        return false;
    });

    function DisplaySimilarHue(hue) {
        // Active all products tab
        OpenProductTabs('all');

        // Apply filter

        $('#filterColorOptionsProduct li').removeClass('activateColor');
        $('#filterColorOptionsProduct li').each(function () {
            if ($(this).find('a').attr('class') == hue) {
                $(this).find('a').addClass('active');
                $(this).toggleClass('activateColor');
            }
            ApplyFilterProduct();
        });
    }
    
    function DisplayProductCode(code) {
        var valThis = code;
        var count = 0;
        $('.active .search-item-parent').each(function () {
            var countDisplay = 0;
            $(this).find('.search-item').each(function () {
                var productName = $(this).attr('productName').toLowerCase();
                var productCode = $(this).attr('productCode').toLowerCase();
                if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
                    countDisplay++;
                    if (countDisplay <= 6 && count < 1) {
                        $(this).show();
                        $(this).removeClass('hideMenu');
                        var countShowed = 0;
                        $(this).find('div[data-src]').each(function () {
                            countShowed++;
                            $(this).nextAll().remove();
                            //$(this).parent().next().find('.cus-pint').html('');
                            if (countShowed <= 6) {
                                $(this).parent().append("<img src='" + $(this).attr('data-src') + "' />");
                                //$(this).parent().next().find('.cus-pint').append("<a href='" + $(this).attr('pintHref') + "' data-pin-do='buttonPin' data-pin-config='above'></a>");
                                //$(this).attr('src', $(this).attr('data-src'));
                            } else {
                                //$(this).find('img').attr('src', '');
                            }
                        });
                    } else {
                        $(this).hide();
                        var itemIndex = parseInt($(this).attr('itemIndex'), 10);
                        if (itemIndex >= 6) {
                            $(this).addClass('hideMenu');
                            $(this).find('img').remove();
                        }
                    }
                }
            });
            if (countDisplay > 0) {
                if (count < 1) {
                    $(this).show();
                    $(this).removeClass('hideMenu');
                } else {
                    $(this).hide();
                    $(this).removeClass('hideMenu');
                    $(this).addClass('hideMenu');
                }
                count++;
            } else {
                $(this).hide();
                $(this).removeClass('hideMenu');
                $(this).addClass('hideMenu');
            }
        });
        //$('.active .search-item').each(function () {
        //    var productName = $(this).attr('productName').toLowerCase();
        //    var productCode = $(this).attr('productCode').toLowerCase();
        //    if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
        //        $(this).show();
        //        count++;
        //    } else {
        //        $(this).hide();
        //    }
        //});

        // Hide gallery collection name in case no sub-product found.
        $('.active .search-item-parent').each(function () {
            var counter = 0;

            $(this).find('.search-item').each(function (i) {
                if ($(this).attr('style') == undefined || $(this).attr('style').indexOf("display: none") == -1) {
                    counter++;
                }
            });

            if (counter == 0) {
                $(this).hide();
            }
        });


        OrganizeGalleryImages();

        DisplayResultSearch(count, valThis);
        
        $(window).unbind('scroll');
    }

    function OpenProductTabs(tab) {
        // Active all products tab
        $('.nav-tabs li').each(function () {
            var attrHref = $(this).find('a').attr('href').toLowerCase();
            if (attrHref.indexOf(tab) > -1) {
                $(this).addClass('active');
                var data = $(this).find('a').attr('data-click');
                var tabLink = $(this).find('a').attr('aria-controls');
                $('.banner-d').each(function () {
                    var databanner = jQuery(this).attr('data-click');
                    if (databanner == data) {
                        $('.banner-d').removeClass('show');
                        $(this).addClass('show');
                    }
                });
                $('.tab-content .tab-pane').each(function () {
                    var tabcontent = $(this).attr('id');
                    if (tabLink == tabcontent) {
                        $('.tab-pane').removeClass('show');
                        $('.tab-pane').removeClass('active');
                        $(this).addClass('show');
                        $(this).addClass('active');
                    }
                });
            } else {
                $(this).removeClass('active');
            }
        });
    }

    function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    ClearFilter();

    function ShowDefaultItem() {
        $(".active").find("li.search-item.default-show").show();
        $(".active").find("div.search-item-parent.default-show").show();

        $(".active").find("li.search-item").nextAll("li").not(".default-show").hide();
        $(".active").find("div.search-item-parent").nextAll("div").not(".default-show").hide();
    }


    // Lazy loading on page
    var timer;
    $(window).scroll(WindowScroll);

    function WindowScroll() {
        if ($(window).scrollTop() + $(window).height() > $(document).height() - 400) {
            if ($(".active").find("li.search-item").nextAll("li").not(".default-show").length > 0) {
                clearTimeout(timer);
                $(".loader").show();
                timer = setTimeout(function () {
                    showNextLoadedItem();
                }, 1000);

            }

            if ($(".active").find("div.search-item-parent").nextAll("div").not(".default-show").length > 0) {
                clearTimeout(timer);
                $(".loader").show();
                timer = setTimeout(function () {
                    showNextLoadedItem();
                }, 1000);

            }
        }
    }

    function showNextLoadedItem() {
        if ($("#filterSizeOptions li.activateSize a").attr('class') != undefined) {
            var option1 = "";
            $("#filterSizeOptions li.activateSize a").each(function () {
                option1 += " " + $(this).attr('class');
            });
        }
        else {
            var option1 = "";
        }

        if ($("#filterColorOptions li.activateColor a").attr('class') != undefined) {
            var option2 = "";
            $("#filterColorOptions li.activateColor a").each(function () {
                option2 += " " + $(this).attr('class');
            });
        } else {
            var option2 = "";
        }

        if ($('#filterColorOptions li.activateColor').length == $('#filterColorOptions li').length-1) {
            option2 = "";
        }
        
        if ($("#filterTypeOptions li.activateType a").attr('class') != undefined) {
            var option3 = "";
            $("#filterTypeOptions li.activateType a").each(function () {
                option3 += " " + $(this).attr('class');
            });
        } else {
            var option3 = "";
        }

        if ($("#filterMaterialOptions li.activateMaterial a").attr('class') != undefined) {
            var option4 = "";
            $("#filterMaterialOptions li.activateMaterial a").each(function () {
                option4 += " " + $(this).attr('class');
            });
        } else {
            var option4 = "";
        }

        option1 = option1.replace(/active/g, "").replace(/  /g, " ").trim();
        option2 = option2.replace(/active/g, "").replace(/  /g, " ").trim();
        option3 = option3.replace(/active/g, "").replace(/  /g, " ").trim();
        option4 = option4.replace(/active/g, "").replace(/  /g, " ").trim();
        
        var counter = 0;
        $(".active").find("li.search-item").nextAll("li").not(".default-show").each(function () {
            $(this).toggleClass("default-show");
            $(this).show();
            counter++;
            if (counter == 10) {
                return false;
            }
        });

        var valThis = $('.search-product').val() !== undefined ? $.trim($('.search-product').val().toLowerCase()) : "";
        if (valThis == "enter product name or code" || valThis == "nhập tên hoặc mã sản phẩm") {
            valThis = "";
        }
        $(".active").find("div.search-item-parent").nextAll("div").not(".default-show").each(function () {
            $(this).toggleClass("default-show");
            var countDisplay = 0;
            $(this).find('div.search-item').each(function () {
                var filterOptions = $(this).attr('filter-options').toLowerCase();
                if (filterOptions.ContainAnyString(option1) && filterOptions.ContainAnyString(option2) && filterOptions.ContainAnyString(option3) && filterOptions.ContainAnyString(option4)) {
                    if (valThis == "") {
                        countDisplay++;
                        if (countDisplay <= 6) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    } else {
                        var productName = $(this).attr('productName').toLowerCase();
                        var productCode = $(this).attr('productCode').toLowerCase();
                        if (productName.indexOf(valThis) > -1 || productCode.indexOf(valThis) > -1) {
                            countDisplay++;
                            if (countDisplay <= 6) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        }
                    }
                }
            });
            if (countDisplay == 0) {
                $(this).hide();
                $(this).removeClass('hideMenu');
                $(this).addClass('hideMenu');
                $(this).find('img').each(function () {
                    $(this).attr('src', '');
                });
            } else {
                $(this).show();
                $(this).removeClass('hideMenu');
                var countShowed = 0;
                $(this).find('div[data-src]').each(function () {
                    countShowed++;
                    $(this).nextAll().remove();
                    //$(this).parent().next().find('.cus-pint').html('');
                    if (countShowed <= 6) {
                        $(this).parent().append("<img src='" + $(this).attr('data-src') + "' />");
                        //$(this).parent().next().find('.cus-pint').append("<a href='" + $(this).attr('pintHref') + "' data-pin-do='buttonPin' data-pin-config='above'></a>");
                        //$(this).attr('src', $(this).attr('data-src'));
                    } else {
                        //$(this).attr('src', '');
                    }
                });
                counter++;
                if (counter == 1) {
                    return false;
                }
            }        
        });
        
        OrganizeGalleryImages();
        //$(".loader").hide();
    }

    // Get hue then filter on products page
    var tech = getUrlParameter('hue');
    if (tech != undefined && tech != '') {
        DisplaySimilarHue(tech.toLowerCase());
    }

    // Open tab on Product and Gallery
    var collection = getUrlParameter('collection');
    if (collection != undefined && collection != '') {
        OpenProductTabs(collection.toLowerCase());
    }

    // Get hue then filter on products page
    var code = getUrlParameter('code');
    if (code != undefined && code != '') {
        OpenProductTabs("all");
        DisplayProductCode(code.toLowerCase());
    }

    $('.custom_pinterest').click(function () {
        $(this).parent().find(".cus-pint a")[0].click();
    });
    
    $('[placeholder]').focus(function () {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
        }
    }).blur(function () {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
        }
    }).blur();
    
    $('.loadMoreGallery').click(function () {
        $(".loader").show();
        $(this).parent().prev().prev().find("div.hideMenu").show();
        $(this).parent().prev().prev().find("div.hideMenu").removeClass('hideMenu');
        $(this).hide();
        $(this).parent().prev().prev().find("div[data-src]").each(function () {
            $(this).nextAll().remove();
            //$(this).parent().next().find('.cus-pint').html('');
            $(this).parent().append("<img src='" + $(this).attr('data-src') + "' />");
            //$(this).parent().next().find('.cus-pint').append("<a href='" + $(this).attr('pintHref') + "' data-pin-do='buttonPin' data-pin-config='above'></a>");
            //$(this).attr('src', $(this).attr('data-src'));
        });
        OrganizeGalleryImages();
    });

    $('.modalPopup').click(function() {
        var target = $(this).attr('data-target');
        $(target).find('div[data-src]').each(function () {
            $(this).nextAll().remove();
            //$(this).parent().next().find('.cus-pint').html('');
            if ($(this).attr('data-src') != undefined) {
                $(this).parent().append("<img src='" + $(this).attr('data-src') + "' />");
                //$(this).parent().next().find('.cus-pint').append("<a href='" + $(this).attr('pintHref') + "' data-pin-do='buttonPin' data-pin-config='above'></a>");
                //$(this).attr('src', $(this).attr('data-src'));
            }
        });
    });
	
});
