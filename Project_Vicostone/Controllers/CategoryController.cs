﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_Vicostone.Models;

namespace Project_Vicostone.Controllers
{
    public class CategoryController : BaseController
    {
        // GET: Category
        QuanLyAccountDbContext _db=new QuanLyAccountDbContext();
        public ActionResult Category(int id)
        {
            var category = _db.Categories.Find(id);
            ViewBag.ListProducts = ListProducts(id);
            return View(category);
        }
           
        public List<Product> ListProducts(int id)
        {
            if (id == 12)
            {
                return _db.Products.ToList();
            }
            var category = _db.Categories.Find(id);
            return _db.Products.Where(x => x.HueId == id || x.Collections == category.NameCategory).ToList();
        }
    }
}