﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_Vicostone.Models;

namespace Project_Vicostone.Controllers
{
    public class GalleryController : BaseController
    {
        // GET: Gallery
        QuanLyAccountDbContext _db=new QuanLyAccountDbContext();
        public ActionResult Gallery()
        {
            return View(_db.Galleries.ToList());
        }

        public ActionResult GalleryToDetail(int id)
        {
            var product = _db.Products.Find(id);

            if (product != null)
            {
                return RedirectToAction("ProductDetail", "Home", new {id = product.Id});
            }
            return RedirectToAction("Gallery","Gallery");
        }

    }
}