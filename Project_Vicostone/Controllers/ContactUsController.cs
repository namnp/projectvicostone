﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_Vicostone.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Project_Vicostone.Common;

namespace Project_Vicostone.Controllers
{
    public class ContactUsController : BaseController
    {
        QuanLyAccountDbContext _db = new QuanLyAccountDbContext();
        // GET: ContactUs

        public ActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContactUs([Bind(Include = "Id,Name,Distributor,Email,Subject,Message")] Contact contacts )
        {
            var response = Request["g-recaptcha-response"];
            string secretKey = "6Ld44zQUAAAAACmUAq2jldHMIQiLtBiycmzYwpPp";
            var client = new WebClient();
            var resual = client.DownloadString(string.Format(
                "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(resual);
            var status = (bool)obj.SelectToken("success");


            if (ModelState.IsValid && status)
            {
                var currentCulture = Session[CommonConstants.CurrentCulture];
                contacts.laguage = currentCulture.ToString();
               
                
                _db.Contacts.Add(contacts);
                _db.SaveChanges();
                return RedirectToAction("ContactUs");
            }
            else
            {
                ModelState.AddModelError("",StaticResources.Resources.InsertContactFailed);
            }
            return View(contacts);
        }
    }
}