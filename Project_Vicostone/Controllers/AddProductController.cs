﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using Project_Vicostone.Models;

namespace Project_Vicostone.Controllers
{
    public class AddProductController : BaseController
    {
        // GET: AddProduct
        private QuanLyAccountDbContext _db = new QuanLyAccountDbContext();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Id,ProductName,ProductCode,Collections,ProductImage,TechnicalInfor,HueId,Material,Sizes")]Product imageModel, HttpPostedFileBase ImageFile)
        {
            string fileName = Path.GetFileNameWithoutExtension(ImageFile.FileName);
            string extension = Path.GetExtension(ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            imageModel.ProductImage = "~/images/image-Product/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/images/image-Product"), fileName);
            ImageFile.SaveAs(fileName);
            if (ModelState.IsValid)
            {
                _db.Products.Add(imageModel);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(imageModel);
        }

        
    }
}