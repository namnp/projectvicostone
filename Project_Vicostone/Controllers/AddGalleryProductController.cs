﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_Vicostone.Models;

namespace Project_Vicostone.Controllers
{
    public class AddGalleryProductController : BaseController
    {
        // GET: AddGalleryProduct
        private QuanLyAccountDbContext _db = new QuanLyAccountDbContext();
        public ActionResult AddGalleryToProduct()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddGalleryToProduct([Bind(Include = "IdGallery,ProductId,ImageGallery")] Gallery galleryModel, HttpPostedFileBase ImageFileGallery)
        {
            string fileName = Path.GetFileNameWithoutExtension(ImageFileGallery.FileName);
            string extension = Path.GetExtension(ImageFileGallery.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            galleryModel.ImageGallery = "~/images/image-Gallery/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/images/image-Gallery"), fileName);
            ImageFileGallery.SaveAs(fileName);

            if (ModelState.IsValid)
            {
                try
                {
                        _db.Galleries.Add(galleryModel);
                        _db.SaveChanges();
                        return RedirectToAction("AddGalleryToProduct");
                    
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting
                            // the current instance as InnerException
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
            }
            return View(galleryModel);
        }
    }
}