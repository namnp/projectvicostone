﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_Vicostone.Models;

namespace Project_Vicostone.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        QuanLyAccountDbContext _db=new QuanLyAccountDbContext();
        public ActionResult Index()
        {
            ViewBag.ListProductIndex = ListGalleries();
            return View();
        }

        public List<Gallery> ListGalleries()
        {
            return _db.Galleries.Take(8).ToList();
        }

        public ActionResult Product()
        {
            return View(_db.Products.ToList());
        }

        public ActionResult ProductDetail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = _db.Products.Find(id);
            if (product==null)
            {
                return HttpNotFound();
            }
            ViewBag.RelatedProducts = ListRelateProducts(id);
            ViewBag.HueProducts = HueProducts(id);
            ViewBag.GalleryProducts = GalleriesProducts(id);
            return View(product);
        }


        public List<Product> ListRelateProducts(int? id)
        {
            var product = _db.Products.Find(id);
            return _db.Products.Where(x => x.Id != id &&x.Collections==product.Collections ).ToList();
        }

        public List<Product> HueProducts(int? id)
        {
            var product = _db.Products.Find(id);
            return _db.Products.Where(x => x.Id != id && x.HueId == product.HueId).ToList();
        }

        public List<Gallery> GalleriesProducts(int? id)
        {
            return _db.Galleries.Where(x => x.ProductId == id).ToList();
        }

        public ActionResult Search(string searchString)
        {
            var products = from p in _db.Products
                           select p;
            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(s => s.ProductName.Contains(searchString) || s.ProductCode.Contains(searchString)||s.Collections.Contains(searchString)
                ||s.Sizes.Contains(searchString)||s.TechnicalInfor.Contains(searchString));
            }
            return View(products);
        }
    }
}