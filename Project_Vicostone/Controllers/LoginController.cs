﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using Project_Vicostone.Common;
using Project_Vicostone.Models;

namespace Project_Vicostone.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login

        //Account account = new Account();
        QuanLyAccountDbContext _db = new QuanLyAccountDbContext();
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Account model)
        {
            var dao = new UserDao();
            if (dao.Login(model.Email, model.Password))
            {
                var email = dao.GetByEmail(model.Email);
                var userSession = new UserLogin();
                userSession.Email = email.Email;
                userSession.Password = email.Password;
                Session.Add(CommonConstants.USER_SESSION, userSession);

                Session["Name"] = "Hi :"+email.FirstName;

                return RedirectToAction("index", "Home");
            }
            ModelState.AddModelError("", "Email or Password not validate");
            return View("Login");
        }


    
      

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "FirstName,LastName,CompanyName,Address,Distributor,City,Country,Email,Telephone,Password,PasswordConfirm")] Account account)
        {
            if (ModelState.IsValid)
            {
                _db.Accounts.Add(account);
                _db.SaveChanges();
                return RedirectToAction("Login");
            }

            return View(account);
        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session["Name"] = null;
            return RedirectToAction("Index", "Home");
        }

    }
}
