﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Project_Vicostone.Models
{
    public class Account
    {
        [StringLength(50)]
        [Required]
        public string FirstName { get; set; }

        [StringLength(50)]
        [Required]
        public string LastName { get; set; }

        [StringLength(250)]
        [Required]
        public string CompanyName { get; set; }

        [StringLength(250)]
        [Required]
        public string Address { get; set; }

        [StringLength(250)]
        public string Distributor { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [Key]
        [Required]
        [StringLength(50)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        [StringLength(20)]
        public string Telephone { get; set; }

        [StringLength(50)]
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [StringLength(50)]
        [Required]
        [Display(Name = "PasswordConfirm")]
        [Compare("Password",ErrorMessage = "The password and confirmtion password do not match.")]
        public string PasswordConfirm { get; set; }

        public bool RememberMe { get; set; }
    }
}