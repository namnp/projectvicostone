﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_Vicostone.Models
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessageResourceName = "Contact_RequiredName",ErrorMessageResourceType = typeof(StaticResources.Resources))]
        [StringLength(20)]
        [Display(Name = "Contact_Name", ResourceType = typeof(StaticResources.Resources))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = "Contact_RequiredDistributor", ErrorMessageResourceType = typeof(StaticResources.Resources))]
        [StringLength(250)]
        [Display(Name = "Contact_Distributor", ResourceType = typeof(StaticResources.Resources))]
        public string Distributor { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Contact_Email", ResourceType = typeof(StaticResources.Resources))]
      //  [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Please enter a valid email address")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessageResourceName = "Contact_RequiredEmail",ErrorMessageResourceType = typeof(StaticResources.Resources))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "Contact_RequiredSubject",ErrorMessageResourceType = typeof(StaticResources.Resources))]
        [Display(Name = "Contact_Subject", ResourceType = typeof(StaticResources.Resources))]
        [StringLength(250)]
        public string Subject { get; set; }

        [Required(ErrorMessageResourceName = "Contact_RequiredMessage", ErrorMessageResourceType = typeof(StaticResources.Resources))]
        [Display(Name = "Contact_Message", ResourceType = typeof(StaticResources.Resources))]
        [StringLength(1000)]
        public string Message { get; set; }
     

        [StringLength(2)]
        public  string laguage { get; set; }
    }
}