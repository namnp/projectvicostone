﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_Vicostone.Models
{
    public class Product
    {
        //Id, ProductName,ProductCode,ImagesProduct
        //TechnicalInfor ,HueId,material ,sizes
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string ProductName { get; set; }

        [Required]
        [StringLength(20)]
        public string ProductCode { get; set; }

        [Required]
        [StringLength(50)]
        public string Collections { get; set; }

        [StringLength(250)]
        [DisplayName("Updoad File")]
        public string ProductImage { get; set; }

        [Required]
        [StringLength(250)]
        public string TechnicalInfor { get; set; }

        [Required]
        public int HueId { get; set; }

        [Required]
        [StringLength(250)]
        public string Material { get; set; }

        [Required]
        [StringLength(50)]
        public string Sizes { get; set; }

        public virtual ICollection<Gallery> Galleries { get; set; }
    }
}