﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project_Vicostone.Models
{
    public class UserDao
    {
        private QuanLyAccountDbContext _db = null;

        public UserDao()
        {
            _db=new QuanLyAccountDbContext();
        }

        public Account GetByEmail(string email)
        {
            return _db.Accounts.SingleOrDefault(x => x.Email == email);
        }
        public bool Login(string email, string password)
        {
            var resual = _db.Accounts.Count(x => x.Email == email && x.Password == password);
            if (resual>0)
            {
                return true;
            }
            return false;
        }
    }
}