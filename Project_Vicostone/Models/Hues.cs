﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_Vicostone.Models
{
    public class Hues
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(250)]
        public string ImageColor { get; set; }
    }
}